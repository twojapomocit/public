#!/bin/sh
# wazuh-deploy.sh
# TwojaPomoc.IT

get_distro() {
  if [ -f /etc/redhat-release ]; then
    RELEASE=/etc/redhat-release
  elif [ -f /etc/SuSE-release ]; then
    RELEASE=/etc/SuSE-release
  elif [ -f /etc/os-release ]; then
    RELEASE=/etc/os-release
  elif [ -f /etc/lsb-release ]; then
    RELEASE=/etc/lsb-release
  elif [ -f /etc/debian_version ]; then
    RELEASE=/etc/debian_version
  else
  	echo "Error: unable to identify operating system"
    exit 1
  fi
  # EL3, EL4, and Fedora
  if egrep -q "^Fedora|release 3|release 4" $RELEASE ; then
    cat /etc/redhat-release
    echo "This platform is no longer supported..." | tee -a $LOG
    echo "Exiting..."
    exit 1
  # EL5
  elif egrep -q "release 5|release 2011" $RELEASE ; then
    DIST="el5"
    DIR=centos/5
  # EL6
  elif egrep -q "release 6|release 2012" $RELEASE ; then
    DIST="el6"
    DIR=centos/6
  elif egrep -q "release 7" $RELEASE ; then
    DIST="el7"
    DIR=centos/7
  elif egrep -q "openSUSE 12" $RELEASE ; then
    DIST="suse12"
    DIR=opensuse/12
  elif egrep -q "openSUSE 13" $RELEASE ; then
    DIST="suse13"
    DIR=opensuse/13
  elif egrep -q "^6.0|squeeze" $RELEASE ; then
    DIST="squeeze"
    DIR=debian/6
  elif egrep -q "wheezy" $RELEASE ; then
    DIST="wheezy"
    DIR=debian/7
  elif egrep -q "jessie" $RELEASE ; then
    DIST="jessie"
    DIR=debian/8
  elif egrep -q "stretch" $RELEASE ; then
    DIST="stretch"
    DIR=debian/9
  elif egrep -q "precise" $RELEASE ; then
    DIST="precise"
    DIR=ubuntu/12
  elif egrep -q "lucid" $RELEASE ; then
    DIST="lucid"
    DIR=ubuntu/10
  elif egrep -q "Raring Ringtail" $RELEASE ; then
    DIST="raring"
    DIR=ubuntu/13
  elif egrep -q "Trusty Tahr" $RELEASE ; then
    DIST="trusty"
    DIR=ubuntu/14
  elif egrep -q "Xenial Xerus" $RELEASE ; then
    DIST="xenial"
    DIR=ubuntu/16
  elif egrep -q "Arch" $RELEASE; then
  	DIST="rolling"
  	DIR=arch
  else
    echo "Error: Unable to determine distribution type." | tee -a $LOG
    exit 1
  fi
}

manager_ip() {
  if [ -z ${WAZUH_MANAGER_ADDR} ]; then
    read -p 'Please enter manager address: ' WAZUH_MANAGER_ADDR
  fi
  sed -i "s/<address>MANAGER_IP<\/address>/<address>${WAZUH_MANAGER_ADDR}<\/address>/g" /var/ossec/etc/ossec.conf
}

get_distro

case $DIST in
	wheezy|jessie|stretch|lucid|raring|trusty|xenial)
		apt-get install -y curl apt-transport-https lsb-release sed && curl -s https://packages.wazuh.com/key/GPG-KEY-WAZUH | apt-key add - && echo "deb https://packages.wazuh.com/3.x/apt/ stable main" | tee /etc/apt/sources.list.d/wazuh.list && apt-get update && apt-get install -y wazuh-agent && manager_ip && /var/ossec/bin/manage_agents && /var/ossec/bin/ossec-control restart
	;;
esac